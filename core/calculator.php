<?php
function result()
{

    if (isset($_POST["calculate"])) {

        $input1 = trim(strip_tags($_POST['input1']));
        $input2 = trim(strip_tags($_POST['input2']));
        $operator = trim(strip_tags($_POST['operator']));
        if (($input2 == 0) and ($operator == 4)) {
            return $result = "На ноль делить нельзя, выберите другое значение";
        } else {
            switch ($operator) {
                case 1:
                    $result = $input1." + " . $input2 . " = " . ( $input1 + $input2);
                    break;
                case 2:
                    $result =$input1." - " . $input2 . " = " . ( $input1 - $input2);
                    break;
                case 3:
                    $result = $input1." * " . $input2 . " = " . ( $input1 * $input2);
                    break;
                case 4:
                    $result = $input1." / " . $input2 . " = " . ( $input1 / $input2);
                    break;
            }
            return $result;
        }
    }

}

//echo result();